<?php

namespace App\DataFixtures;

use App\Entity\Department;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $this->loadDepartments($manager);
        $this->loadUsers($manager);
    }

    private function loadDepartments(ObjectManager $manager)
    {
        $departments = ['backend-development department', 'frontend-development department', 'web-test department'];

        foreach ($departments as $title) {
            $department = new Department();
            $department->setTitle($title);
            $manager->persist($department);
            $this->addReference($title, $department);
        }
        $manager->flush();
    }

    private function loadUsers(ObjectManager $manager)
    {
        $users = $this->getUserData();

        foreach ($users as $i => [$pin, $fullName, $email, $phone, $address, $status]) {
            $user = new User();
            $user->setPin($pin);
            $user->setFullName($fullName);
            $user->setEmail($email);
            $user->setPhone($phone);
            $user->setAddress($address);
            $user->setStatus($status);
            $department = $this->getReference(['backend-development department', 'frontend-development department','web-test department'][0 === $i ? 0 : rand(0, 2)]);
            $user->setDepartment($department);

            $manager->persist($user);
        }

        $manager->flush();
    }

    private function getUserData(): array
    {
        return [
            // $userData = [$pin, fullname, $email, $phone, $address, $status];
            ['1210819758910015','Pupkin Alexandr Sergeevich', 'alexpup@gmail.com','0772154865','Kyrgyz Republic, Bishkek city, Ahunbaeva, 191', 1],
            ['1210719888910115','Pupkin Sergey Sergeevich', 'serhey@gmail.com','0772154864','Kyrgyz Republic, Bishkek city, Ahunbaeva, 120', 1],
            ['1080919818910115','Berezin Alexey Sergeevich', 'alexber@gmail.com','0772154866','Kyrgyz Republic, Bishkek city, Ahunbaeva, 170', 1],
            ['1212345678910115','Malahov Vasiliy Sergeevich', 'misha@gmail.com','0772154869','Kyrgyz Republic, Bishkek city, Ahunbaeva, 160', 1],
            ['1212345678910110','Shenchenko Petr Sergeevich', 'petr@gmail.com','0772154860','Kyrgyz Republic, Bishkek city, Ahunbaeva, 150', 1],
            ['1212345678910117','Maslyakov Ivan Sergeevich', 'ivan@gmail.com','0772154766','Kyrgyz Republic, Bishkek city, Ahunbaeva, 191', 1],
            ['1212345678910119','Vasilenko Dmitri Sergeevich', 'dima@gmail.com','0772152866','Kyrgyz Republic, Bishkek city, Ahunbaeva, 189', 0],
            ['1210619868910115','Peskov Alexandr Sergeevich', 'alexpes@gmail.com','0772134866','Kyrgyz Republic, Bishkek city, Ahunbaeva, 112', 0],
            ['1150719778910118','Kozlovskiy Alexandr Sergeevich', 'kozalex@gmail.com','0772124866','Kyrgyz Republic, Bishkek city, Ahunbaeva, 123', 0],
        ];
    }
}
