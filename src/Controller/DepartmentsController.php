<?php

namespace App\Controller;

use App\Entity\Department;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * @Route("/api", name="api_")
 */
class DepartmentsController extends AbstractFOSRestController
{
    /**
     * @Rest\Get("/departments")
     *
     */
    public function getDepartments()
    {
        $departments = $this->getDoctrine()->getRepository(Department::class)->findAll();
        return $this->handleView($this->view($departments));
    }

    /**
     * @Rest\Get("/department/{id}")
     * @param Department $department
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getDepartment(Department $department)
    {
        return $this->handleView($this->view($department));
    }

}
