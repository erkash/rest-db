<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * @Route("/api", name="api_")
 */
class UsersController extends AbstractFOSRestController
{
    /**
     * @Rest\Get("/users")
     */
    public function getUsers()
    {
        $users = $this->getDoctrine()->getRepository(User::class)->findAll();
        return $this->handleView($this->view($users));
    }

    /**
     * @Rest\Get("/user/{pin}")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getUserByPin($pin)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['pin' => $pin]);
        $view = $this->view($user);
        $view->getContext()->setGroups(['default']);
        return $this->handleView($view);
    }
}
